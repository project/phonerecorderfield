PhoneRecorderField.module
--------------------------------------------------------------------------------
This module provides a new CCK field that calls the user to record his message using VoIP API. 

The new field is based on AudioField module and records audio via the VoIP API.

--------------------------
Installation

1. Extract PhoneRecorderField to your sites/all/modules directory. Make sure you have installed VoIP,CCK, FileField and AudioField modules.
2. Enable the PhoneRecorderField module in admin/build/modules.
3. Choose any content type from admin/content/types and go to "manage fields".
4. Add a new field, select File as its field type and Phone Recorder as the widget.
5. Save.
6. Create new content and you will see the Phone Recorder!

--------------------------
Serve audio from 3rd party

By default audio recordings are copied from 3rd party to local server and served from there. However you can serve audio recordings directly from 3rd party 
server(Twilio) by following this steps:

1. Choose your content type with PhoneRecorderField and  go to "display fields".
2. Select "Audio from external URL" as display for your PhoneRecorderField.

FileField Sources support

This module also adds new "Phone Recorder" uploading method for FileField Sources module. Its possible to create one field with multiple uploading choices.
1. Download and install FileField Sources module.
2. Change your PhoneRecorder widget to Audio Upload widget.
3. Under File Sources fieldset you will see various uploading methods (Phone Recorder method is added by this module).
---
The PhoneRecorderField module has been originally developed by Tamer Zoubi under the sponsorship of the MIT Center for Future Civic Media (http://civic.mit.edu).




